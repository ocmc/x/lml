package lml

type TemplateDate struct {
	Day int
	Month int
	Year int
}
func Left(l LangType, s ... string) string {
	return ""
}
func Center(l LangType, s ... string) string {
	return ""
}
func Right(l LangType, s ... string) string {
	return ""
}
type LangType int
const (
	L1 LangType = iota
	L2
	L3
)
type TemplateType int
const (
	Block TemplateType = iota
	Book
	Service
)
type Header struct {
	Type TemplateType
	Date TemplateDate
	HeaderEven string
	HeaderOdd string
	FooterEven string
	FooterOdd string
	PageNbr int
}
var Template Header

func RID(class, id string) string {
	return ""
}
func SID(class, id string) string {
	return ""
}
func NID(class, t string) string {
	return ""
}
func Insert(id string) string {
	return ""
}
func P(class string, id ... string ) string {
	return ""
}
func _PageNbr() string {
	return ""
}
func _Date() string {
	return ""
}
type DOW int
const (
	SUN DOW = iota
	MON
	TUE
	WED
	THU
	FRI
	SAT
)
func DayOfWeek() DOW {
	return MON
}
type DOM int
const (
	D1 DOM = iota + 1
	D2
	D3
	D4
	D5
	D6
	D7
	D8
	D9
	D10
	D11
	D12
	D13
	D14
	D15
	D16
	D17
	D18
	D19
	D20
	D21
	D22
	D23
	D24
	D25
	D26
	D27
	D28
	D29
	D30
	D31
)
func DayOfMonth() DOM {
	return D1
}
type MONTH int
const (
	JAN MONTH = iota + 1
	FEB
	MAR
	APR
	MAY
	JUN
	JUL
	AUG
	SEP
	OCT
	NOV
	DEC
)
func MonthOfYear() MONTH {
	return JAN
}
